#include "ast.h"
#include <stdio.h>

expressionTree operatorExpression(opType op, expressionTree left, expressionTree right)
{
	expressionTree retval = (expressionTree) malloc(sizeof(struct expression));
	retval->kind = operatorExp;
	retval->data.oper.op = op;
	retval->data.oper.left = left;
	retval->data.oper.right = right;
	return retval;
}

expressionTree identifierExpression(char *variable)
{
	expressionTree retval = (expressionTree) malloc(sizeof(struct expression));
	retval->kind = varaiableExp;
	retval->data.varaiable = variable;
	return retval;
}

expressionTree constatnExpression(int constantVal)
{
	expressionTree retval = (expressionTree) malloc(sizeof(struct expression));
	retval->kind = constantExp;
	retval->data.varaiable = constantVal;
	return retval;
}
