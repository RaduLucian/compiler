
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 1 "comp.y"

//YACC functions
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "ast.h"

//stack values
typedef union
{
	int i;
	char* s;
} tipst;

#define YYSTYPE tipst

int linie=1,nvar=1,nvaraux=0,varaux[32];
char denvar[32][20];
FILE *fisin,*fisout;

void yyerror(char *s)
{
	printf("Eroare linia %d: %s\n",linie,s);
	getch();
	exit(1);
}

int varauxnoua()
{
	int v;
	if (nvar+nvaraux>=32) { yyerror("Nu mai exista registrii liberi."); }
	v=31;
	while (v>0)
	{
		if (!varaux[v]) break;
		v--;
	}
	varaux[v]=1; nvaraux++;
	return v;
}

void scoatevaraux(int v)
{
	varaux[v]=0;
}

int variabila(char *s)
{
	int i;
	for (i=0;i<nvar;i++) if (!strcmp(s,denvar[i])) return i;
	if (nvar+nvaraux>=32) { yyerror("Nu mai exista registrii liberi."); }
	strcpy(denvar[nvar++],s);
	return nvar-1;
}


/* Line 189 of yacc.c  */
#line 132 "comp.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NUMAR = 258,
     CUVANT = 259,
     EGAL = 260,
     PUNCTSIVIRGULA = 261,
     SLASH = 262,
     STARTBRACE = 263,
     ENDBRACE = 264,
     STARTCURLYBRACE = 265,
     ENDCURLYBRACE = 266,
     IF = 267,
     ELSE = 268,
     WHILE = 269,
     FOR = 270,
     NOTEQUAL = 271,
     EQUAL = 272,
     GREATER = 273,
     GREATEROREQUAL = 274,
     LOWER = 275,
     LOWEROREQUAL = 276,
     OR = 277,
     AND = 278,
     MINUS = 279,
     PLUS = 280,
     IMPARTIRE = 281,
     INMULTIRE = 282,
     UNARYMINUS = 283
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 202 "comp.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   236

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  30
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  18
/* YYNRULES -- Number of rules.  */
#define YYNRULES  68
/* YYNRULES -- Number of states.  */
#define YYNSTATES  183

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   283

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      29,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     7,    10,    15,    20,    25,    27,
      30,    32,    35,    37,    40,    45,    50,    54,    58,    63,
      69,    77,    85,    95,   105,   117,   123,   131,   141,   153,
     157,   161,   165,   169,   173,   177,   181,   185,   189,   193,
     197,   201,   205,   209,   213,   217,   221,   225,   229,   233,
     237,   241,   245,   249,   253,   257,   261,   265,   269,   273,
     277,   281,   283,   285,   289,   293,   297,   301,   304
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      31,     0,    -1,    -1,    31,    29,    -1,    31,    42,    -1,
       4,     5,    47,     6,    -1,     4,     5,    47,     6,    -1,
       4,     5,    47,     6,    -1,    42,    -1,    42,    35,    -1,
      42,    -1,    42,    36,    -1,    42,    -1,    42,    37,    -1,
       4,     5,    47,     6,    -1,     4,     5,    47,     6,    -1,
       4,     5,    47,    -1,     4,     5,    47,    -1,     4,     5,
      47,     6,    -1,    12,     8,    46,     9,    34,    -1,    12,
       8,    45,     9,    32,    13,    33,    -1,    12,     8,    46,
       9,    10,    35,    11,    -1,    12,     8,    45,     9,    10,
      35,    11,    13,    33,    -1,    12,     8,    45,     9,    42,
      13,    10,    35,    11,    -1,    12,     8,    45,     9,    10,
      35,    11,    13,    10,    35,    11,    -1,    14,     8,    44,
       9,    38,    -1,    14,     8,    44,     9,    10,    37,    11,
      -1,    15,     8,    40,     6,    43,     6,    41,     9,    39,
      -1,    15,     8,    40,     6,    43,     6,    41,     9,    10,
      36,    11,    -1,    47,    16,    47,    -1,    47,    17,    47,
      -1,    47,    18,    47,    -1,    47,    19,    47,    -1,    47,
      20,    47,    -1,    47,    21,    47,    -1,    43,    23,    43,
      -1,    43,    22,    43,    -1,    47,    16,    47,    -1,    47,
      17,    47,    -1,    47,    18,    47,    -1,    47,    19,    47,
      -1,    47,    20,    47,    -1,    47,    21,    47,    -1,    44,
      23,    44,    -1,    44,    22,    44,    -1,    47,    16,    47,
      -1,    47,    17,    47,    -1,    47,    18,    47,    -1,    47,
      19,    47,    -1,    47,    20,    47,    -1,    47,    21,    47,
      -1,    45,    23,    45,    -1,    45,    22,    45,    -1,    47,
      16,    47,    -1,    47,    17,    47,    -1,    47,    18,    47,
      -1,    47,    19,    47,    -1,    47,    20,    47,    -1,    47,
      21,    47,    -1,    46,    23,    46,    -1,    46,    22,    46,
      -1,     3,    -1,     4,    -1,    47,    25,    47,    -1,    47,
      24,    47,    -1,    47,    27,    47,    -1,    47,    26,    47,
      -1,    24,    47,    -1,     8,    47,     9,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    69,    69,    69,    70,    72,    84,    98,   109,   112,
     117,   121,   125,   129,   133,   146,   159,   172,   185,   196,
     200,   203,   207,   210,   213,   216,   219,   222,   226,   230,
     233,   236,   242,   249,   256,   263,   266,   272,   275,   278,
     284,   291,   298,   305,   308,   313,   316,   319,   325,   332,
     339,   346,   349,   355,   358,   361,   367,   374,   381,   388,
     391,   395,   396,   397,   421,   451,   475,   499,   515
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NUMAR", "CUVANT", "EGAL",
  "PUNCTSIVIRGULA", "SLASH", "STARTBRACE", "ENDBRACE", "STARTCURLYBRACE",
  "ENDCURLYBRACE", "IF", "ELSE", "WHILE", "FOR", "NOTEQUAL", "EQUAL",
  "GREATER", "GREATEROREQUAL", "LOWER", "LOWEROREQUAL", "OR", "AND",
  "MINUS", "PLUS", "IMPARTIRE", "INMULTIRE", "UNARYMINUS", "'\\n'",
  "$accept", "linie", "instrIf", "instrIfAndElse", "instrWithoutIf",
  "blockInstr", "instrBlockFor", "blockInstrWhile", "instrWhile",
  "instrFor", "initializeFor", "incrementareFor", "instr", "boolenFor",
  "booleanWhile", "booleanElse", "boolean", "expr", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,    10
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    30,    31,    31,    31,    32,    33,    34,    35,    35,
      36,    36,    37,    37,    38,    39,    40,    41,    42,    42,
      42,    42,    42,    42,    42,    42,    42,    42,    42,    43,
      43,    43,    43,    43,    43,    43,    43,    44,    44,    44,
      44,    44,    44,    44,    44,    45,    45,    45,    45,    45,
      45,    45,    45,    46,    46,    46,    46,    46,    46,    46,
      46,    47,    47,    47,    47,    47,    47,    47,    47
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     2,     2,     4,     4,     4,     1,     2,
       1,     2,     1,     2,     4,     4,     3,     3,     4,     5,
       7,     7,     9,     9,    11,     5,     7,     9,    11,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     1,     1,     3,     3,     3,     3,     2,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     0,     1,     0,     0,     0,     0,     3,     4,     0,
       0,     0,     0,    61,    62,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    67,    18,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    68,    64,    63,    66,    65,
       0,     0,     0,     0,    52,     0,    51,     0,     0,    19,
      60,     0,    59,    45,    46,    47,    48,    49,    50,     0,
       0,    25,    44,    43,    37,    38,    39,    40,    41,    42,
      16,     0,     0,     0,     0,     8,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    12,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     9,     0,    20,     0,
      45,    46,    47,    48,    49,    50,     0,    21,    53,    54,
      55,    56,    57,    58,     0,    26,    13,     0,     0,    36,
      35,    29,    30,    31,    32,    33,    34,     5,     0,     0,
       0,     7,    14,     0,     0,     0,    22,     0,    23,    17,
       0,     0,    27,     0,     6,     0,     0,    10,    24,     0,
      28,    11,    15
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,    62,   128,    69,    94,   176,   113,    81,   172,
      24,   148,    95,    91,    21,    18,    19,    22
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -67
static const yytype_int16 yypact[] =
{
     -67,     1,   -67,    -2,    17,    25,    27,   -67,   -67,     3,
       3,     3,    39,   -67,   -67,     3,     3,    46,    31,    81,
     138,   113,   161,     7,    70,    -5,   -67,   -67,     3,     3,
       3,     3,   133,     3,     3,     4,     3,     3,     3,     3,
       3,     3,     3,     3,    22,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,   -67,   -17,   -17,   -67,   -67,
      64,    63,    78,    85,    43,   173,    43,    76,    63,   -67,
     111,   185,   111,   148,   148,   148,   148,   148,   148,    95,
      63,   -67,   146,   146,   148,   148,   148,   148,   148,   148,
     148,    74,   197,     3,    91,    63,   101,   136,     3,     3,
       3,     3,     3,     3,     3,   112,     3,     3,     3,     3,
       3,     3,     3,   155,    63,   145,     3,     3,     3,     3,
       3,     3,     3,     3,    58,   171,   -67,   222,   -67,    63,
     148,   148,   148,   148,   148,   148,    62,   -67,   148,   148,
     148,   148,   148,   148,    68,   -67,   -67,   223,   220,   203,
     203,   148,   148,   148,   148,   148,   148,   -67,    24,     3,
     219,   -67,   -67,     3,   134,    63,   -67,   115,   -67,   148,
     226,    63,   -67,   221,   -67,     3,   224,    63,   -67,   126,
     -67,   -67,   -67
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -67,   -67,   -67,    75,   -67,   -66,    57,   122,   -67,   -67,
     -67,   -67,    -1,    44,   150,   174,   183,     8
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
       8,     2,   105,     9,    55,     3,    13,    14,    67,    30,
      31,    15,    53,     4,    68,     5,     6,    17,    20,    28,
      29,    30,    31,    25,    26,    10,    79,    16,   127,   126,
       7,    63,    80,    11,   165,    12,    56,    57,    58,    59,
      32,    65,    65,    23,    71,    71,    73,    74,    75,    76,
      77,    78,    27,    33,    34,    84,    85,    86,    87,    88,
      89,    90,    92,   160,   157,    33,    34,     3,   161,    93,
      28,    29,    30,    31,   162,     4,    54,     5,     6,   114,
     115,   104,    28,    29,    30,    31,    28,    29,    30,    31,
      35,    96,    28,    29,    30,    31,   116,   117,    97,   173,
     112,   124,   125,    36,    37,   127,   130,   131,   132,   133,
     134,   135,   136,   114,   138,   139,   140,   141,   142,   143,
     144,   174,    44,   137,    92,    92,   151,   152,   153,   154,
     155,   156,   182,    36,    37,    45,    46,    60,   170,    28,
      29,    30,    31,    61,   171,     4,   129,     5,     6,   147,
      28,    29,    30,    31,    38,    39,    40,    41,    42,    43,
     149,   150,    28,    29,    30,    31,   145,   167,    45,    46,
     177,   169,    28,    29,    30,    31,   177,    47,    48,    49,
      50,    51,    52,   179,   158,    28,    29,    30,    31,    98,
      99,   100,   101,   102,   103,    82,    83,    28,    29,    30,
      31,   106,   107,   108,   109,   110,   111,    64,    66,    28,
      29,    30,    31,   118,   119,   120,   121,   122,   123,    70,
      72,    28,    29,    30,    31,   116,   117,   159,   163,   164,
     168,   175,   178,   166,   181,   180,   146
};

static const yytype_uint8 yycheck[] =
{
       1,     0,    68,     5,     9,     4,     3,     4,     4,    26,
      27,     8,     5,    12,    10,    14,    15,     9,    10,    24,
      25,    26,    27,    15,    16,     8,     4,    24,     4,    95,
      29,    32,    10,     8,    10,     8,    28,    29,    30,    31,
       9,    33,    34,     4,    36,    37,    38,    39,    40,    41,
      42,    43,     6,    22,    23,    47,    48,    49,    50,    51,
      52,    53,    54,   129,     6,    22,    23,     4,     6,     5,
      24,    25,    26,    27,     6,    12,     6,    14,    15,    80,
       6,     5,    24,    25,    26,    27,    24,    25,    26,    27,
       9,    13,    24,    25,    26,    27,    22,    23,    13,   165,
       5,    93,    11,    22,    23,     4,    98,    99,   100,   101,
     102,   103,   104,   114,   106,   107,   108,   109,   110,   111,
     112,     6,     9,    11,   116,   117,   118,   119,   120,   121,
     122,   123,     6,    22,    23,    22,    23,     4,     4,    24,
      25,    26,    27,    10,    10,    12,    10,    14,    15,     4,
      24,    25,    26,    27,    16,    17,    18,    19,    20,    21,
     116,   117,    24,    25,    26,    27,    11,   159,    22,    23,
     171,   163,    24,    25,    26,    27,   177,    16,    17,    18,
      19,    20,    21,   175,    13,    24,    25,    26,    27,    16,
      17,    18,    19,    20,    21,    45,    46,    24,    25,    26,
      27,    16,    17,    18,    19,    20,    21,    33,    34,    24,
      25,    26,    27,    16,    17,    18,    19,    20,    21,    36,
      37,    24,    25,    26,    27,    22,    23,     5,     5,     9,
      11,     5,    11,   158,   177,    11,   114
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    31,     0,     4,    12,    14,    15,    29,    42,     5,
       8,     8,     8,     3,     4,     8,    24,    47,    45,    46,
      47,    44,    47,     4,    40,    47,    47,     6,    24,    25,
      26,    27,     9,    22,    23,     9,    22,    23,    16,    17,
      18,    19,    20,    21,     9,    22,    23,    16,    17,    18,
      19,    20,    21,     5,     6,     9,    47,    47,    47,    47,
       4,    10,    32,    42,    45,    47,    45,     4,    10,    34,
      46,    47,    46,    47,    47,    47,    47,    47,    47,     4,
      10,    38,    44,    44,    47,    47,    47,    47,    47,    47,
      47,    43,    47,     5,    35,    42,    13,    13,    16,    17,
      18,    19,    20,    21,     5,    35,    16,    17,    18,    19,
      20,    21,     5,    37,    42,     6,    22,    23,    16,    17,
      18,    19,    20,    21,    47,    11,    35,     4,    33,    10,
      47,    47,    47,    47,    47,    47,    47,    11,    47,    47,
      47,    47,    47,    47,    47,    11,    37,     4,    41,    43,
      43,    47,    47,    47,    47,    47,    47,     6,    13,     5,
      35,     6,     6,     5,     9,    10,    33,    47,    11,    47,
       4,    10,    39,    35,     6,     5,    36,    42,    11,    47,
      11,    36,     6
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:

/* Line 1455 of yacc.c  */
#line 69 "comp.y"
    { linie++; printf("La line nu crapa\n"); ;}
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 70 "comp.y"
    { linie++; printf("La line instr nu crapa\n"); ;}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 72 "comp.y"
    {
			printf("instrIF nu crapa\n");
			int v;
			if ((yyvsp[(3) - (4)]).s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			strcpy((yyvsp[(3) - (4)]).s,(yyvsp[(3) - (4)]).s+1);
			v=atoi((yyvsp[(3) - (4)]).s);
			if (v>nvar) scoatevaraux(v);
			free((yyvsp[(1) - (4)]).s); free((yyvsp[(3) - (4)]).s);
			fprintf(fisout,"\tj \tEndIf\n");
		;}
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 84 "comp.y"
    {
			fprintf(fisout,"Else:\n");
			printf("instrIFElse nu crapa\n");
			int v;
			if ((yyvsp[(3) - (4)]).s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			strcpy((yyvsp[(3) - (4)]).s,(yyvsp[(3) - (4)]).s+1);
			v=atoi((yyvsp[(3) - (4)]).s);
			if (v>nvar) scoatevaraux(v);
			free((yyvsp[(1) - (4)]).s); free((yyvsp[(3) - (4)]).s);
			fprintf(fisout,"\tj \tEndif\n");
		;}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 98 "comp.y"
    {
			printf("La CUVANT = expr; nu crapa\n");
			int v;
			if ((yyvsp[(3) - (4)]).s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			strcpy((yyvsp[(3) - (4)]).s,(yyvsp[(3) - (4)]).s+1);
			v=atoi((yyvsp[(3) - (4)]).s);
			if (v>nvar) scoatevaraux(v);
			free((yyvsp[(1) - (4)]).s); free((yyvsp[(3) - (4)]).s);
		;}
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 109 "comp.y"
    {
			printf("BlockInstr\n");
		;}
    break;

  case 9:

/* Line 1455 of yacc.c  */
#line 112 "comp.y"
    {
			printf("BlockInstr\n");
			fprintf(fisout,"\tj \tEndif\n");
		;}
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 117 "comp.y"
    {
			printf("BlockInstr FOR\n");
			fprintf(fisout,"\tj \tLoop\n");
		;}
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 121 "comp.y"
    {
			printf("BlockInstr\n");
		;}
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 125 "comp.y"
    {
			printf("BlockInstr\n");
			fprintf(fisout,"\tj \tLoop\n");
		;}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 129 "comp.y"
    {
			printf("BlockInstr\n");
		;}
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 134 "comp.y"
    {
			printf("La CUVANT = expr; nu crapa\n");
			int v;
			if ((yyvsp[(3) - (4)]).s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			strcpy((yyvsp[(3) - (4)]).s,(yyvsp[(3) - (4)]).s+1);
			v=atoi((yyvsp[(3) - (4)]).s);
			if (v>nvar) scoatevaraux(v);
			free((yyvsp[(1) - (4)]).s); free((yyvsp[(3) - (4)]).s);
			fprintf(fisout,"\tj \tLoop\n");
		;}
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 147 "comp.y"
    {
			printf("La CUVANT = expr; nu crapa\n");
			int v;
			if ((yyvsp[(3) - (4)]).s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			strcpy((yyvsp[(3) - (4)]).s,(yyvsp[(3) - (4)]).s+1);
			v=atoi((yyvsp[(3) - (4)]).s);
			if (v>nvar) scoatevaraux(v);
			free((yyvsp[(1) - (4)]).s); free((yyvsp[(3) - (4)]).s);
			fprintf(fisout,"\tj \tLoop\n");
		;}
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 160 "comp.y"
    {
			printf("Am pus LOOP\n");
			int v;
			if ((yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila((yyvsp[(1) - (3)]).s),(yyvsp[(3) - (3)]).s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila((yyvsp[(1) - (3)]).s),(yyvsp[(3) - (3)]).s);
			strcpy((yyvsp[(3) - (3)]).s,(yyvsp[(3) - (3)]).s+1);
			v=atoi((yyvsp[(3) - (3)]).s);
			if (v>nvar) scoatevaraux(v);
			free((yyvsp[(1) - (3)]).s); free((yyvsp[(3) - (3)]).s);
			fprintf(fisout,"Loop:\n");
		;}
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 173 "comp.y"
    {
			printf("La CUVANT = expr; nu crapa\n");
			int v;
			if ((yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila((yyvsp[(1) - (3)]).s),(yyvsp[(3) - (3)]).s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila((yyvsp[(1) - (3)]).s),(yyvsp[(3) - (3)]).s);
			strcpy((yyvsp[(3) - (3)]).s,(yyvsp[(3) - (3)]).s+1);
			v=atoi((yyvsp[(3) - (3)]).s);
			if (v>nvar) scoatevaraux(v);
			free((yyvsp[(1) - (3)]).s); free((yyvsp[(3) - (3)]).s);
		;}
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 186 "comp.y"
    {
			printf("La CUVANT = expr; nu crapa\n");
			int v;
			if ((yyvsp[(3) - (4)]).s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila((yyvsp[(1) - (4)]).s),(yyvsp[(3) - (4)]).s);
			strcpy((yyvsp[(3) - (4)]).s,(yyvsp[(3) - (4)]).s+1);
			v=atoi((yyvsp[(3) - (4)]).s);
			if (v>nvar) scoatevaraux(v);
			free((yyvsp[(1) - (4)]).s); free((yyvsp[(3) - (4)]).s);
		;}
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 196 "comp.y"
    {printf("IF 1\n");
											fprintf(fisout,"\tj \tEndIf\n"); 
											fprintf(fisout,"EndIf:\n"); 
										;}
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 200 "comp.y"
    {printf("IF 2\n");
														fprintf(fisout,"EndIf:\n"); 
													;}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 203 "comp.y"
    {printf("IF 3\n");
															fprintf(fisout,"\tj \tEndIf\n"); 
															fprintf(fisout,"EndIf:\n");
														;}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 207 "comp.y"
    {printf("IF 4\n");
																						fprintf(fisout,"EndIf:\n"); 
																					;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 210 "comp.y"
    {printf("IF 5\n");
																						fprintf(fisout,"EndIf:\n"); 
																					;}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 213 "comp.y"
    {printf("IF 6\n");
																						fprintf(fisout,"EndIf:\n"); 
																					;}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 216 "comp.y"
    {printf("WHILE 1\n");
											fprintf(fisout,"EndWhile:\n"); 
										;}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 219 "comp.y"
    {printf("WHILE 2\n");
																			fprintf(fisout,"EndWhile:\n"); 
																		;}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 222 "comp.y"
    {printf("FOR 1\n");
																			fprintf(fisout,"EndFor:\n");
																		;}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 226 "comp.y"
    {printf("FOR 2\n");
																			fprintf(fisout,"EndFor:\n");
																		;}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 230 "comp.y"
    {printf("booleanFor 1\n");
								fprintf(fisout,"\n\tbne \t%s,%s,EndFor\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 233 "comp.y"
    {printf("booleanFor 2\n");
								fprintf(fisout,"\n\tbeq \t%s,%s,EndFor\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 236 "comp.y"
    {printf("booleanFor 3\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\n\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndFor\n",v,1); 			
							;}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 242 "comp.y"
    {printf("booleanFor 4\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\n\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndFor\n",v,1);
								fprintf(fisout,"\tbeq \t$%s,%s,EndFor\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s)
							;}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 249 "comp.y"
    {printf("booleanFor 5\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\n\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndFor\n",v,0); 
								
							;}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 256 "comp.y"
    {printf("booleanFor 6\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\n\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndFor\n",v,0);
								fprintf(fisout,"\tbeq \t$%s,%s,EndFor\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 263 "comp.y"
    {printf("booleanFor 7\n");
								
							;}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 266 "comp.y"
    {printf("booleanFor 8\n");
								
							;}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 272 "comp.y"
    {printf("booleanElse 1\n");
								fprintf(fisout,"Loop:\n\tbne \t%s,%s,EndWhile\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 275 "comp.y"
    {printf("booleanElse 2\n");
								fprintf(fisout,"Loop:\n\tbeq \t%s,%s,EndWhile\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 278 "comp.y"
    {printf("booleanElse 3\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"Loop:\n\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndWhile\n",v,1); 			
							;}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 284 "comp.y"
    {printf("booleanElse 4\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"Loop:\n\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndWhile\n",v,1);
								fprintf(fisout,"\tbeq \t$%s,%s,EndWhile\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s)
							;}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 291 "comp.y"
    {printf("booleanElse 5\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"Loop:\n\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndWhile\n",v,0); 
								
							;}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 298 "comp.y"
    {printf("booleanElse 6\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"Loop:\n\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndWhile\n",v,0);
								fprintf(fisout,"\tbeq \t$%s,%s,EndWhile\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 305 "comp.y"
    {printf("booleanElse 7\n");
								
							;}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 308 "comp.y"
    {printf("booleanElse 8\n");
								
							;}
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 313 "comp.y"
    {printf("booleanElse 1\n");
								fprintf(fisout,"\tbne \t%s,%s,Else\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 316 "comp.y"
    {printf("booleanElse 2\n");
								fprintf(fisout,"\tbeq \t%s,%s,Else\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 319 "comp.y"
    {printf("booleanElse 3\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,Else\n",v,1); 			
							;}
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 325 "comp.y"
    {printf("booleanElse 4\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,Else\n",v,1);
								fprintf(fisout,"\tbeq \t$%s,%s,Else\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s)
							;}
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 332 "comp.y"
    {printf("booleanElse 5\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,Else\n",v,0); 
								
							;}
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 339 "comp.y"
    {printf("booleanElse 6\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,Else\n",v,0);
								fprintf(fisout,"\tbeq \t$%s,%s,Else\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 346 "comp.y"
    {printf("booleanElse 7\n");
								
							;}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 349 "comp.y"
    {printf("booleanElse 8\n");
	
	
							;}
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 355 "comp.y"
    {printf("boolean 1\n");
								fprintf(fisout,"\tbne \t%s,%s,EndIf\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 358 "comp.y"
    {printf("boolean 2\n");
								fprintf(fisout,"\tbeq \t%s,%s,EndIf\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 361 "comp.y"
    {printf("boolean 3\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndIf\n",v,1); 
							;}
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 367 "comp.y"
    {printf("boolean 4\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndIf\n",v,1); 
								fprintf(fisout,"\tbeq \t$%s,%s,EndIf\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 374 "comp.y"
    {printf("boolean 5\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndIf\n",v,0); 
								
							;}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 381 "comp.y"
    {printf("boolean 6\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndIf\n",v,0);
								fprintf(fisout,"\tbeq \t$%s,%s,EndIf\n",(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
							;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 388 "comp.y"
    {printf("boolean 7\n");
	
							;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 391 "comp.y"
    {printf("boolean 8\n");
	
							;}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 395 "comp.y"
    { printf("La expr : NUMAR nu crapa\n"); (yyval).s=(char *)malloc(20); sprintf((yyval).s,"%d",(yyvsp[(1) - (1)]).i);  ;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 396 "comp.y"
    { (yyval).s=(char *)malloc(20); sprintf((yyval).s,"$%d",variabila((yyvsp[(1) - (1)]).s)); free((yyvsp[(1) - (1)]).s); printf("La expr : CUVANT nu crapa"); ;}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 397 "comp.y"
    {
				printf("La expr : expr PLUS expr nu crapa\n");
				int v;
				if ((yyvsp[(1) - (3)]).s[0]=='$' || (yyvsp[(3) - (3)]).s[0]=='$') v=varauxnoua();
 				(yyval).s=(char *)malloc(20);
				if ((yyvsp[(1) - (3)]).s[0]=='$' && (yyvsp[(3) - (3)]).s[0]!='$') fprintf(fisout,"\taddi\t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]=='$' && (yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\tadd \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]!='$' && (yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\taddi\t$%d,%s,%s\n",v,(yyvsp[(3) - (3)]).s,(yyvsp[(1) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]!='$' && (yyvsp[(3) - (3)]).s[0]!='$') sprintf((yyval).s,"%d",atoi((yyvsp[(1) - (3)]).s)+atoi((yyvsp[(3) - (3)]).s));
				else sprintf((yyval).s,"$%d",v);
				if ((yyvsp[(1) - (3)]).s[0]=='$')
				{
					strcpy((yyvsp[(1) - (3)]).s,(yyvsp[(1) - (3)]).s+1);
					v=atoi((yyvsp[(1) - (3)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				if ((yyvsp[(3) - (3)]).s[0]=='$')
				{
					strcpy((yyvsp[(3) - (3)]).s,(yyvsp[(3) - (3)]).s+1);
					v=atoi((yyvsp[(3) - (3)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				free((yyvsp[(1) - (3)]).s); free((yyvsp[(3) - (3)]).s);
			;}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 421 "comp.y"
    {
				printf("La expr : expr MINUS expr nu crapa\n");
				int v,v2;
				if ((yyvsp[(1) - (3)]).s[0]=='$' || (yyvsp[(3) - (3)]).s[0]=='$') v=varauxnoua();
 				(yyval).s=(char *)malloc(20);
				if ((yyvsp[(1) - (3)]).s[0]=='$' && (yyvsp[(3) - (3)]).s[0]!='$') fprintf(fisout,"\taddi\t$%d,%s,-%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]=='$' && (yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\tsub \t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]!='$' && (yyvsp[(3) - (3)]).s[0]=='$')
				{
					v2=varauxnoua();
					fprintf(fisout,"\taddi\t$%d,$0,%s\n",v2,(yyvsp[(1) - (3)]).s);
					fprintf(fisout,"\tsub\t$%d,$%d,%s\n",v,v2,(yyvsp[(3) - (3)]).s);
					scoatevaraux(v2);
				}
				if ((yyvsp[(1) - (3)]).s[0]!='$' && (yyvsp[(3) - (3)]).s[0]!='$') sprintf((yyval).s,"%d",atoi((yyvsp[(1) - (3)]).s)-atoi((yyvsp[(3) - (3)]).s));
				else sprintf((yyval).s,"$%d",v);
				if ((yyvsp[(1) - (3)]).s[0]=='$')
				{
					strcpy((yyvsp[(1) - (3)]).s,(yyvsp[(1) - (3)]).s+1);
					v=atoi((yyvsp[(1) - (3)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				if ((yyvsp[(3) - (3)]).s[0]=='$')
				{
					strcpy((yyvsp[(3) - (3)]).s,(yyvsp[(3) - (3)]).s+1);
					v=atoi((yyvsp[(3) - (3)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				free((yyvsp[(1) - (3)]).s); free((yyvsp[(3) - (3)]).s);
			;}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 451 "comp.y"
    {
				printf("La expr : expr INMULTIRE expr nu crapa\n");
				int v;
				if ((yyvsp[(1) - (3)]).s[0]=='$' || (yyvsp[(3) - (3)]).s[0]=='$') v=varauxnoua();
 				(yyval).s=(char *)malloc(20);
				if ((yyvsp[(1) - (3)]).s[0]=='$' && (yyvsp[(3) - (3)]).s[0]!='$') fprintf(fisout,"\tmuli\t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]=='$' && (yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\tmult\t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]!='$' && (yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\tmuli\t$%d,%s,%s\n",v,(yyvsp[(3) - (3)]).s,(yyvsp[(1) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]!='$' && (yyvsp[(3) - (3)]).s[0]!='$') sprintf((yyval).s,"%d",atoi((yyvsp[(1) - (3)]).s)*atoi((yyvsp[(3) - (3)]).s));
				else sprintf((yyval).s,"$%d",v);
				if ((yyvsp[(1) - (3)]).s[0]=='$')
				{
					strcpy((yyvsp[(1) - (3)]).s,(yyvsp[(1) - (3)]).s+1);
					v=atoi((yyvsp[(1) - (3)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				if ((yyvsp[(3) - (3)]).s[0]=='$')
				{
					strcpy((yyvsp[(3) - (3)]).s,(yyvsp[(3) - (3)]).s+1);
					v=atoi((yyvsp[(3) - (3)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				free((yyvsp[(1) - (3)]).s); free((yyvsp[(3) - (3)]).s);
			;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 475 "comp.y"
    {
				printf("La expr : expr INMULTIRE expr nu crapa\n");
				int v;
				if ((yyvsp[(1) - (3)]).s[0]=='$' || (yyvsp[(3) - (3)]).s[0]=='$') v=varauxnoua();
 				(yyval).s=(char *)malloc(20);
				if ((yyvsp[(1) - (3)]).s[0]=='$' && (yyvsp[(3) - (3)]).s[0]!='$') fprintf(fisout,"\tdivi\t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]=='$' && (yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\tdiv\t$%d,%s,%s\n",v,(yyvsp[(1) - (3)]).s,(yyvsp[(3) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]!='$' && (yyvsp[(3) - (3)]).s[0]=='$') fprintf(fisout,"\tdivi\t$%d,%s,%s\n",v,(yyvsp[(3) - (3)]).s,(yyvsp[(1) - (3)]).s);
				if ((yyvsp[(1) - (3)]).s[0]!='$' && (yyvsp[(3) - (3)]).s[0]!='$') sprintf((yyval).s,"%d",atoi((yyvsp[(1) - (3)]).s)/atoi((yyvsp[(3) - (3)]).s));
				else sprintf((yyval).s,"$%d",v);
				if ((yyvsp[(1) - (3)]).s[0]=='$')
				{
					strcpy((yyvsp[(1) - (3)]).s,(yyvsp[(1) - (3)]).s+1);
					v=atoi((yyvsp[(1) - (3)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				if ((yyvsp[(3) - (3)]).s[0]=='$')
				{
					strcpy((yyvsp[(3) - (3)]).s,(yyvsp[(3) - (3)]).s+1);
					v=atoi((yyvsp[(3) - (3)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				free((yyvsp[(1) - (3)]).s); free((yyvsp[(3) - (3)]).s);
			;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 500 "comp.y"
    {
				printf("La expr : MINUS expr nu crapa\n");
				int v=varauxnoua();
 				(yyval).s=(char *)malloc(20);
				if ((yyvsp[(2) - (2)]).s[0]=='$')
				{
					fprintf(fisout,"\tsub\t$%d,$0,%s\n",v,(yyvsp[(2) - (2)]).s);
					sprintf((yyval).s,"$%d",v);
					strcpy((yyvsp[(2) - (2)]).s,(yyvsp[(2) - (2)]).s+1);
					v=atoi((yyvsp[(2) - (2)]).s);
					if (v>nvar) scoatevaraux(v);
				}
				else sprintf((yyval).s,"%d",-atoi((yyvsp[(2) - (2)]).s));
				free((yyvsp[(2) - (2)]).s);
			;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 515 "comp.y"
    { printf("La expr: STARTBRACE expr ENDBRACE\n"); (yyval).s=(yyvsp[(2) - (3)]).s; ;}
    break;



/* Line 1455 of yacc.c  */
#line 2361 "comp.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 518 "comp.y"


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void main(int narg,char *argv[])
{
	int i;
	if (narg!=3) init(NULL,NULL,&fisin,&fisout);
	else init(argv[1],argv[2],&fisin,&fisout);
	for (i=0;i<32;i++) varaux[i]=0;
	yyparse();
}
