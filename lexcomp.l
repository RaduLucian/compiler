%{
//stack values
typedef union
{
	int i;
	char* s;
} tipst;

#define YYSTYPE tipst

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "comp.tab.h"

extern YYSTYPE yylval;

void init(char *nfin,char *nfout,FILE **fisin,FILE **fisout)
{
	char numef[100];
	if (nfin) strcpy(numef,nfin);
	else
	{
		printf("Dati numele fisierului de intrare: ");
		gets(numef);
	}
	yyin=fopen(numef,"r");
	*fisin=yyin;
	if (!yyin)
	{
		printf("Nu am putut deschide fisierul de intrare.");
		exit(1);
	}
	if (nfout) strcpy(numef,nfout);
	else
	{
		printf("Dati numele fisierului de iesire:  ");
		gets(numef);
	}
	yyout=fopen(numef,"w");
	*fisout=yyout;
	if (!yyout)
	{
		printf("Nu am putut crea fisierul de iesire.");
		exit(1);
	}
}

%}

cifra [0-9]
nrIntreg {cifra}+

%option noyywrap

%%

"else"       			{printf("ELSE \n"); return ELSE; }
"altfel"       			{printf("ELSE \n"); return ELSE; }
"if"          		    {printf("IF \n"); return IF; }
"daca"					{printf("IF \n"); return IF; }
"while"			        {printf("WHILE \n"); return WHILE; }
"cattimp"				{printf("WHILE \n"); return WHILE; }
"for"					{printf("FOR \n");return FOR;}	
"pentru"				{printf("FOR \n");return FOR;}
"=="					{printf("EQUAL \n"); return EQUAL; }
"!="					{printf("NOTEQUAL \n"); return NOTEQUAL; }
">="					{printf("GREATEROREGUAL \n"); return GREATEROREQUAL; }
"<="					{printf("LOWEROREQUAL \n"); return LOWEROREQUAL; }
"<"						{printf("LOWER \n"); return LOWER; }
">"						{printf("GREATER \n"); return GREATER; }
"||"					{printf("OR \n"); return OR; }
"&&"					{printf("AND \n"); return AND; }
"=" 					{printf("EGAL \n"); return EGAL;}
";" 					{printf("PUNCTSIVIRGULA \n"); return PUNCTSIVIRGULA;}
"+"						{printf("PLUS \n"); return PLUS; }
"-"						{printf("MINUS \n"); return MINUS; }
"/" 					{printf("IMPARTIRE \n"); return IMPARTIRE; }
"*"						{printf("INMULTIRE \n"); return INMULTIRE; }
"("						{printf("STARTBRACE \n"); return STARTBRACE; }
")" 					{printf("ENDBRACE \n"); return ENDBRACE; }
"{"						{printf("STARTCURLYBRACE \n"); return STARTCURLYBRACE; }
"}"						{printf("ENDCURLYBRACE \n"); return ENDCURLYBRACE; }
{nrIntreg}				{printf("NUMAR\n"); yylval.i=atoi(yytext); return NUMAR; } 
[a-zA-Z]|[0-9a-zA-Z]+	{printf("CUVANT \n"); yylval.s=(char *)malloc(sizeof(char)*(strlen(yytext)+1)); strcpy(yylval.s,yytext); return CUVANT; }
[\n]                	{}
[ ]+             		{}
[\t]                	{}

%%