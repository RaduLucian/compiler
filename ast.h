#ifndef AST_H
#define AST_H

typedef struct expression *expressionTree;

typedef enum {PlusOp, MinusOp, TimpesOp, DivideOp} opType;

struct expression
{
    enum {operatorExp, constatnExp, variableExp} kind;
    union
    {
        int constantVal;
        char* variable;
        struct
        {
			opType op;
            expressionTree left;
            expressionTree right;
        } oper;
    } data;
};

expressionTree operatorExpression(opType op, expressionTree left, expressionTree right);
expressionTree identifierExpression(char *variable);
expressionTree constatnExpression(int constantVal);
#endif